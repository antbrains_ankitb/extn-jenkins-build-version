package com.kristasoft.extension.deployment.util;

import java.io.FileWriter;
import java.io.IOException;

public class FileUtil {

    public static void writeString(String path, String content){
        try(FileWriter writer = new FileWriter(path)) {
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}