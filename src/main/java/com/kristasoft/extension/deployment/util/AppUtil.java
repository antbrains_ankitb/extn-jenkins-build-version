package com.kristasoft.extension.deployment.util;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class AppUtil {
    public static String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 4) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }

    public static Map<String, String> getRequestParameters(HttpServletRequest request) {
            Map<String, String> modelMap = new HashMap<>();
            try {
                Enumeration enumeration = request.getParameterNames();

                while (enumeration.hasMoreElements()) {
                    String parameterName = (String) enumeration.nextElement();
                    modelMap.put(parameterName, request.getParameter(parameterName));
                }
            } catch (Exception e) {
                System.out.println("The exception is" + e.getMessage());
            }
            return modelMap;
        }
}