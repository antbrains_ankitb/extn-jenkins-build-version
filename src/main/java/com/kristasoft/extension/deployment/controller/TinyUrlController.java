package com.kristasoft.extension.deployment.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kristasoft.extension.deployment.util.AppUtil;
import com.kristasoft.extension.deployment.util.FileUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tiny")
public class TinyUrlController {

    @Value("${tiny.url.path}")
    private String filePath = "/var/app/extension/";
    @Value("${tiny.url.file.name}")
    private String fileName = "tinyurl.txt";
    public static final Type TYPE = new TypeToken<Map<String, String>>() {
    }.getType();

    @PostMapping
    public ResponseEntity create(@RequestParam("path") String path) throws IOException {
        File f = new File(filePath + fileName);
        if (!f.exists()) {
            f.createNewFile();
        }
        String s = new String(Files.readAllBytes(Paths.get(filePath + fileName)));
        Map<String, String> urlMapping = new HashMap<>();
        if (s.trim().length() > 0) {
            urlMapping = new Gson().fromJson(s, TYPE);
        }
        String tinyUrl = AppUtil.getSaltString();
        urlMapping.put(tinyUrl, path);
        FileUtil.writeString(filePath + fileName, new Gson().toJson(urlMapping));
        return ResponseEntity.ok().body(tinyUrl);
    }

    @GetMapping
    public ResponseEntity get(@RequestParam("tiny") String tiny) throws IOException {
        String s = new String(Files.readAllBytes(Paths.get(filePath + fileName)));
        Map<String, String> urlMapping = new HashMap<>();
        if (s.trim().length() > 0) {
            urlMapping = new Gson().fromJson(s, TYPE);
        }
        return ResponseEntity.ok().body(urlMapping.getOrDefault(tiny, "Path Not found"));
    }

    @GetMapping("/all")
    public ResponseEntity get() throws IOException {
        String s = new String(Files.readAllBytes(Paths.get(filePath + fileName)));
        Map<String, String> urlMapping = new HashMap<>();
        if (s.trim().length() > 0) {
            urlMapping = new Gson().fromJson(s, TYPE);
        }
        return ResponseEntity.ok().body(new Gson().toJson(urlMapping));
    }


    @DeleteMapping
    public ResponseEntity delete(@RequestParam("tiny") String tiny) throws IOException {
        String s = new String(Files.readAllBytes(Paths.get(filePath + fileName)));
        Map<String, String> urlMapping = new HashMap<>();
        if (s.trim().length() > 0) {
            urlMapping = new Gson().fromJson(s, TYPE);
        }
        if (urlMapping.containsKey(tiny)) {
            urlMapping.remove(tiny);
            FileUtil.writeString(filePath + fileName, new Gson().toJson(urlMapping));
            return ResponseEntity.ok().body("Deleted Successfully");
        } else return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Path not found");
    }

    @PostMapping("/execute")
    public ResponseEntity execute(@RequestParam("tiny") String tiny, @RequestBody Object body) throws IOException {
        String s = new String(Files.readAllBytes(Paths.get(filePath + fileName)));
        Map<String, String> urlMapping = new HashMap<>();
        if (s.trim().length() > 0) {
            urlMapping = new Gson().fromJson(s, TYPE);
        }
        String path = urlMapping.getOrDefault(tiny, null);
        if (path != null) {
            RestTemplate restTemplate = new RestTemplate();
            Map<String, Object> req = new HashMap<>();
            Map<String, String> base64 = new HashMap<>();
            base64.put("base64encoded", new Gson().toJson(body));
            req.put("payload", base64);
            ResponseEntity<String> response = restTemplate.postForEntity(path, new Gson().toJson(req), String.class);
            return ResponseEntity.ok(response.getBody());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Path not found");
    }


    @PostMapping("/executeraw")
    public ResponseEntity executeRaw(@RequestParam("tiny") String tiny, @RequestBody Object body) throws IOException {
        String s = new String(Files.readAllBytes(Paths.get(filePath + fileName)));
        Map<String, String> urlMapping = new HashMap<>();
        if (s.trim().length() > 0) {
            urlMapping = new Gson().fromJson(s, TYPE);
        }
        String path = urlMapping.getOrDefault(tiny, null);
        if (path != null) {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.postForEntity(path, new Gson().toJson(body), String.class);
            return ResponseEntity.ok(response.getBody());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Path not found");
    }

    @PostMapping("/raw")
    public ResponseEntity<String> raw(@RequestParam("tiny") String tiny, HttpServletRequest request) throws IOException {
        String s = new String(Files.readAllBytes(Paths.get(filePath + fileName)));
        Map<String, String> urlMapping = new HashMap<>();
        if (s.trim().length() > 0) {
            urlMapping = new Gson().fromJson(s, TYPE);
        }
        String path = urlMapping.getOrDefault(tiny, null);

        String body = "Body is empty";
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        }
        Map<String, String> params = AppUtil.getRequestParameters(request);
        System.out.println("Body is" + body);
        System.out.println("RequestParams is" + new Gson().toJson(params));
        RestTemplate restTemplate = new RestTemplate();
        Map<String, Object> req = new HashMap<>();
        Map<String, String> base64 = new HashMap<>();
        base64.put("base64encoded", body);
        req.put("payload", base64);
        ResponseEntity<String> response = restTemplate.postForEntity(path, new Gson().toJson(req), String.class);
        return ResponseEntity.ok(response.getBody());
    }


}