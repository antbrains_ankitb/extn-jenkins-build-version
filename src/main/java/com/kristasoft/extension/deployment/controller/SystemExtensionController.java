package com.kristasoft.extension.deployment.controller;

import com.google.gson.Gson;
import com.kristasoft.extension.deployment.util.AppUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("ext")
public class SystemExtensionController {

    @GetMapping
    public ResponseEntity<String> get(@RequestParam("path") String path) {
        return ResponseEntity.ok(path);
    }


    @PostMapping
    public ResponseEntity<String> post(@RequestParam("path") String path, @RequestBody Object body) {
        RestTemplate restTemplate = new RestTemplate();
        Map<String, Object> req = new HashMap<>();
        Map<String, String> base64 = new HashMap<>();
        base64.put("base64encoded", new Gson().toJson(body));
        req.put("payload", base64);
        ResponseEntity<String> response = restTemplate.postForEntity(path, new Gson().toJson(req), String.class);
        return ResponseEntity.ok(response.getBody());
    }

    @PostMapping("/deploy")
    public ResponseEntity<String> post(@RequestParam("path") String path) {
        RestTemplate restTemplate = new RestTemplate();
        Map<String, Object> req = new HashMap<>();
        Map<String, String> base64 = new HashMap<>();
        base64.put("base64encoded", "base64");
        req.put("payload", base64);
        ResponseEntity<String> response = restTemplate.postForEntity(path, new Gson().toJson(req), String.class);
        return ResponseEntity.ok(response.getBody());
    }


    @PostMapping("/raw")
    public ResponseEntity<String> raw(@RequestParam("path") String path, HttpServletRequest request) throws IOException {
        String body = "Body is empty";
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        }
        Map<String, String> params = AppUtil.getRequestParameters(request);
        System.out.println("Body is" + body);
        System.out.println("RequestParams is" + new Gson().toJson(params));
        RestTemplate restTemplate = new RestTemplate();
        Map<String, Object> req = new HashMap<>();
        Map<String, String> base64 = new HashMap<>();
        base64.put("base64encoded", body);
        req.put("payload", base64);
        ResponseEntity<String> response = restTemplate.postForEntity(path, new Gson().toJson(req), String.class);
        return ResponseEntity.ok(response.getBody());
    }




}