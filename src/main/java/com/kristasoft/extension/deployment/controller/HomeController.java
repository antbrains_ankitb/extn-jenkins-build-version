package com.kristasoft.extension.deployment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @Autowired
    BuildProperties buildProperties;
    
    @GetMapping("/")
    public String index(Model model){
        model.addAttribute("buildVersion", buildProperties.getVersion());
        model.addAttribute("buildName", buildProperties.getName());
        model.addAttribute("buildTime", buildProperties.getTime());
        return "index";

    }

}